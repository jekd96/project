package controllers;


import models.TableName;
import play.libs.Json;
import play.mvc.*;

import java.util.List;


/**
 * Created by Admin on 17.06.2015.
 */
public class Application extends Controller {
    public static Result index() {
        List<TableName> list = TableName.find.all();
        return ok(Json.toJson(list));
    }
}
