package models;


import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by Admin on 15.07.2015.
 */
@Entity
public class TableName extends Model{

    @Id
    public Long id;

    @Column(name = "name")
    public String name;

    public static Finder<Long, TableName> find = new Finder<Long, TableName>(
            Long.class, TableName.class
    );

}
